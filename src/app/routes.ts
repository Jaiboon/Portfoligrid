import { Routes } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { ResumeComponent } from "./resume/resume.component";
import { WorkComponent } from "./work/work.component";
import { ContactComponent } from "./contact/contact.component";
import { SigninComponent } from "./signin/signin.component";
import { SignupComponent } from "./signup/signup.component";
import { AdminComponent } from "./admin/admin.component";
import { ManageComponent } from "./manage/manage/manage.component";
export const appRoutes: Routes = [
  { path: "home", component: HomeComponent },
  { path: "resume", component: ResumeComponent },
  { path: "work", component: WorkComponent },
  { path: "contact", component: ContactComponent },
  { path: "signin", component: SigninComponent },
  { path: "signup", component: SignupComponent },
  { path: "admin", component: AdminComponent },
  { path: "manage", component: ManageComponent },

  { path: "**", redirectTo: "home", pathMatch: "full" }
];
